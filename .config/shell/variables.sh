#!/bin/sh

export PATH="$PATH:$HOME/.config/shell/scripts:$HOME/.config/shell/scripts/arch"

export DOTNET_CLI_TELEMENTRY_OPTOUT=1

export EDITOR="nvim"
export BROWSER="firefox"
export TERMINAL="st"

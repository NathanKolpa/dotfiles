#!/bin/sh

[ -x "$(command -v nvim)" ] && alias vim="nvim"

alias \
	ls="ls -hN --color=auto --group-directories-first" \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -vI" \
	sctl="systemctl" \
	jctl="journalctl" \
	dotgit='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME' \
	mkd="mkdir -pv"

efvf() {
	fzf | xargs $EDITOR
}

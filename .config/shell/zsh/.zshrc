source $HOME/.config/shell/variables.sh
source $HOME/.config/shell/aliases.sh

#history
HISTFILE=$HOME/.cache/zsh/history
HISTSIZE=100
SAVEHIST=100

#autocomple
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

#colors
autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

#plugins
#todo: maybe replace with fast-syntax-highlighting?
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

ZSH_AUTOSUGGEST_COMPLETION_IGNORE="**pacman -S*(y|u) *"
ZSH_AUTOSUGGEST_STRATEGY=(completion history)
ZSH_AUTOSUGGEST_USE_ASNYC=1
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

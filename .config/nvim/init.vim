" ===== Essential
set hidden
set mouse=n
set ma

" Use ctrl-[hjkl] to select the active
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

" ===== Bloat stuff

call plug#begin(stdpath('data') . '/plugged')
Plug 'neoclide/coc.nvim', {'branch': 'release', 'do': 'npm i --frozen-lockfile'}
Plug 'norcalli/nvim-colorizer.lua'
Plug 'scrooloose/nerdtree'
Plug 'preservim/nerdcommenter'
Plug 'ayu-theme/ayu-vim'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'Chiel92/vim-autoformat'
call plug#end()


" looks
if(has("termguicolors"))
	set termguicolors
endif

set number
set laststatus=2
set list
set listchars=tab:>-,trail:.,extends:>,precedes:<
set background=dark " maybe move to light
let ayucolor="mirage"
colorscheme ayu


lua require'colorizer'.setup()

let coc_start_at_startup = 0

function InitCodeEditor()

	let g:coc_start_at_startup = 1

	" tab completion
	inoremap <silent><expr> <TAB>
				\ pumvisible() ? "\<C-n>" :
				\ <SID>check_back_space() ?  "\<TAB>" :
				\ coc#refresh()
	inoremap <expr><S-TAB> pumvisible() ? "\<C-P>" : "\<C-h>"

	function! s:check_back_space() abort
		let col = col('.') - 1
		return !col || getline('.')[col - 1] =~# '\s'
	endfunction

	" ctrl space
	inoremap <silent><expr> <c-space> coc#refresh()

	" nerd tree
	autocmd StdinReadPre * let s:std_in=1

	let g:NERDTreeIgnore = ['^node_modules$', '^vendor$']

	" layout for opened folders
	if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in")
		autocmd VimEnter * exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0]
	endif

	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

	" nerd commenter
	filetype plugin on
	nmap <C-_>   <Plug>NERDCommenterToggle
	vmap <C-_>   <Plug>NERDCommenterToggle<CR>gv

	" Format
	let g:python3_host_prog="/usr/bin/python3.9"
	au BufWrite * :Autoformat
endfunction

call InitCodeEditor()
